# [zdex-downloadgitrepo](https://gitee.com/evilrule/zdex-downloadgitrepo)

NodeJS git repository 下载工具，支持 GitHub, Gitee, GitLab, Bitbucket

## 安装

    $ npm install zdex-downloadgitrepo

## API

### download(repository, destination, options, callback)

下载 git `repository` 到目标文件夹 `destination`。可以配置 `options` 参数，下载完成后调用回调函数 `callback`。

#### repository

仓库地址简写字符串：

- **GitHub** - `github:owner/name` 或者 `owner/name`
- **Gitee** - `gitee:owner/name` 
- **GitLab** - `gitlab:owner/name`
- **Bitbucket** - `bitbucket:owner/name`

`repository` 参数默认使用 `master` 分支，你可以通过使用 URL 片段来指定分支 `owner/name#my-branch`。

为了指定下类型，你可以指定一个自定义来源：`gitlab:custom.com:owner/name`。

除非指定了协议, 否则自定义来源将分别默认为 `https` 或 `git @`

除了使用仓库地址简写字符串来指定 git hosts，你也可直接使用仓库地址：

- **Direct** - `direct:url`

使用 direct 将绕过 仓库地址简写字符串 解释器，直接使用 `url` 地址。
如果在非克隆的情况下使用 `direct`， 则必使用完整 zip 文件 url ，如果需要，请指定包括分支的完整路径。
如果在克隆的情况下使用 `direct`，你必须指定 git 仓库的完整地址，可以用 URL 片段指定分支，格式形如: `direct:url#my-branch`

#### destination
下载 repository 的存储目标地址。

#### options

一个可选的参数对象，可设参数包括：

- `clone` - {Boolean} 默认值 `false` - true 表示使用 `git clone` 替代 http 下载；虽然速度可能会稍慢一些, 但如果设置了适当的 SSH 密钥， 则允许使用私有仓库 （private repositories）。

#### callback
形如  `function (err)` 的回调函数。

## Examples

### 仓库地址简写字符串（Shorthand ）

使用 http 方式从 Github repository 的 master 分支下载。

```javascript
download('flipxfx/download-git-repo-fixture', 'test/tmp', function (err) {
  console.log(err ? 'Error' : 'Success')
})
```

使用 git clone 方式从 Gitee repository 的 master 分支下载。

```javascript
download('gitee:evilrule/zdcg-template-common', 'test/tmp', { clone: true }, function (err) {
  console.log(err ? 'Error' : 'Success')
})
```

使用 git clone 方式从 Bitbucket repository 的 my-branch 分支下载。

```javascript
download('bitbucket:flipxfx/download-git-repo-fixture#my-branch', 'test/tmp', { clone: true }, function (err) {
  console.log(err ? 'Error' : 'Success')
})
```

使用 http  方式从 GitLab repository 的 my-branch 分支下载。

```javascript
download('gitlab:mygitlab.com:flipxfx/download-git-repo-fixture#my-branch', 'test/tmp', function (err) {
  console.log(err ? 'Error' : 'Success')
})
```

使用 git clone 方式从自定义来源的和协议的 GitLab repository 下载。

注意：如果从自定义来源的仓库克隆， 仓储类型（`github`,`gitlab` 等）是不需要指定的。

```javascript
download('https://mygitlab.com:flipxfx/download-git-repo-fixture#my-branch', 'test/tmp', { clone: true }, function (err) {
  console.log(err ? 'Error' : 'Success')
})
```

### 直接使用 url（Direct）

直接使用 url 通过 http 下载。

```javascript
download('direct:https://gitlab.com/flipxfx/download-git-repo-fixture/repository/archive.zip', 'test/tmp', function (err) {
  console.log(err ? 'Error' : 'Success')
})
```

使用 url 通过 http 下载 master 分支。

```javascript
download('direct:https://gitlab.com/flipxfx/download-git-repo-fixture.git', 'test/tmp', { clone: true }, function (err) {
  console.log(err ? 'Error' : 'Success')
})
```

使用 url 通过 git clone 下载 my-branch 分支。

```javascript
download('direct:https://gitlab.com/flipxfx/download-git-repo-fixture.git#my-branch', 'test/tmp', { clone: true }, function (err) {
  console.log(err ? 'Error' : 'Success')
})
```

## License

MIT

## 声明

本库基于 [download-git-repo](git://github.com/flipxfx/download-git-repo)  1.1.0 版本扩展开发。

